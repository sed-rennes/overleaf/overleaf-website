This notice applies to the Overleaf service hosted at the URL:
https://overleaf.irisa.fr and coined as Overleaf@irisa in the following.


The instance is hosted at Irisa Lab at Rennes (France).

=== "Usage policy"

    Overleaf@irisa.fr is used by many people, and we are proud of the trust
    placed in us. In exchange, we trust you to use the services we provide
    responsibly.

    You agree not to misuse the Overleaf@irisa.fr services. For example, you
    must not, and must not attempt to, do any of the following things using, or
    in respect of, any part of Overleaf@irisa.fr:

    - probe, scan, or test the vulnerability of any system or network;
    - tamper with, breach or otherwise circumvent any security or authentication
    measure;
    - access or use any non-public area / content or shared area you have not
    been invited to;
    - interfere with or disrupt any user, host, or network, for example by
    overloading, flooding, spamming, or mail-bombing;
    - introduce, or otherwise use to facilitate the spread of, spyware or any
    other malware;
    - send unsolicited communications or spam;
    - send altered, deceptive or false source-identifying information, including
    “spoofing” or “phishing”;
    - promote or advertise products or services;
    - impersonate or misrepresent your affiliation with any person or entity;
    - upload, publish or share anything that is unlawful, fraudulent,
    misleading, infringes or violates another's rights, pornographic, indecent,
    false, defamatory, derogatory, threatening, obscene, abusive, offensive,
    hateful, inflammatory or is likely to harass, upset, annoy, alarm, embarrass
    or invade the privacy of, any person, deceitful, promotes or advocates an
    unlawful act or activity, discriminatory, sexually explicit or violent;
    - upload, publish or share health or other sensitive personal information,
    classified, controlled unclassified or other regulated information,
    educational records, personal information of others, or anything which you
    are not authorised to upload, publish or share;
    - infringe any trademark, copyright (including design rights), database
    rights, or other intellectual property rights, or privacy or other rights,
    of any other person or breach any contractual or other legal duty; - violate
    or cause the violation of the law in any way;
    - sell, rent, lease, license, frame, commercialize or use for the benefit of
    any other person;
    - copy, modify, adapt or create derivative works, or decipher, decompile,
    disassemble, reverse engineer or attempt to derive any source code or
    underlying ideas or algorithms.

=== "Cookie policy"

    We use “cookies”. A cookie is a small data file that we transfer to your
    device. We may use “persistent cookies” to save your registration ID and
    login password for future logins to Overleaf. We may use “session ID
    cookies” to enable certain features of Overleaf, to better understand how
    you interact with it and to monitor aggregate usage and web traffic routing.
    You can instruct your browser, by changing its options, to stop accepting
    cookies or to prompt you before accepting a cookie from the websites you
    visit. If you do not accept cookies, however, you may not be able to use all
    aspects of Overleaf.


=== "Privacy notice"

    This Notice explains what personal data we collect, how we may use and
    manage it and the rights you may have in relation to such personal data.
    When we refer to “personal data” in this Notice, we mean information
    relating to an identified or identifiable individual that we collect and use
    in connection with Overleaf@irisa

    ### How do we collect and use personal data?

    - information you provide to us directly online. For example, when you
    register to use Overleaf@irisa, complete one of our web forms or make a support
    request, we collect the personal data you provide, like your name, email
    address and other basic contact details / professional information.

    - information we collect from your use of Overleaf@irisa. When you use
    Overleaf, we may collect information about that usage and other technical
    information, such as your IP address, browser type and any referring website
    addresses. We may combine this automatically collected log information to
    build aggregated usage statistics.


    ### Retention of your personal data

    We only keep your personal data for as long as it is necessary for the
    purposes for which it was collected, after which it will be destroyed,
    erased or anonymised. For example, if you are an Overleaf@irisa user, we
    will delete inactive projects after a grace period of 400 days. Account with
    no attached project will be erased periodically.