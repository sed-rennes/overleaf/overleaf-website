You'll find some of the most frequently asked questions about the
overleaf installation and its specificity.

- The service is hosted here: [https://overleaf.irisa.fr](https://overleaf.irisa.fr)

- For a more general introduction to overleaf please refer to the official
  documentation :
  [https://www.overleaf.com/learn/](https://www.overleaf.com/learn/)

- Support & questions can be sent to [support-overleaf@inria.fr](mailto:support-overleaf@inria.fr) / mattermost : [https://mattermost.inria.fr/sharelatex/](https://mattermost.inria.fr/sharelatex/)
 
- Frequently asked questions: [FAQ](faq.md)

- We try to keep an up-to-date events list that concerns the service here: [Events](events.md)



