# In progress


---

# Planned

## 20236030 gitlab unreachable
 
gitlab.inria.fr will be unreachable from 30. June 2023 to 3. July 2023. Since overleaf.irisa.fr authentication is based on gitlab.inria.fr, you may be unable to connect to the service during this period: [more info here(internal link)](https://doc-si.inria.fr/pages/viewpage.action?pageId=74622385) 


---
# Done

- [12. April 2023]: The service will be unreachable
  - we change the underlying storage server

- [14. Mar 2023]: The server will be restarted, this will lead to the following changes
    - compile timeout will be set to 6 min instead of 3 min currently
    - texlive 2022 compile environment will be available
    - (admin) compilation service healthchecks
- [9. Dec 2022] A new reverse proxy has been deployed in front of the service
- [30 Nov 2022] We are experiencing transient connnections issues. We are investigating.
- [24. nov 2022] The service will be restarted Tuesday 29. Nov at 12pm. This should take only few minutes.
- [15. nov 2022] Gitlab upgrade wed. 16 november betwenn 1:30 pm and 2pm (this will impact authentication)

- [10 oct. 2022]: overleaf will be in maintenance 
    - from Thursday 20. Oct 2022 8am 
    - until Thursday 20. Oct 2022 12pm

- ⚠️ From 2022/09/26 18pm to 2022/09/27 12pm the service will be stopped (maintenance).  

- [16. Aug 2022] Since 14. August 2022 the service is unstable due to a power outage in Rennes site
  - [19. Aug 2022] we are investigating a DNS issue within the machine

- [07. Aug 2022] overleaf.irisa.fr has been redeployed and is open for beta testing. During this phase you can start to use it on a daily basis, on our side we'll monitor the service before a general opening (most likely in September 2022).


- ⚠️ From the 24 June 2022 to the 04. July of 2022 this service won't be reachable.

  🚀 During this time we'll proceed to the deployment of the service in its final version.

  ❗⚠️ Note that we don't plan to migrate the projects that are currently stored, please backup them if you want to keep them (you'll be able to re-upload them in the new version though).

  Please reach us at: support-overleaf@inria.fr or https://mattermost.inria.fr/sharelatex/
